# Database Project #
## PubLib.zero ##

**Technologies that was used:**
>1. Java 8 and Intellij IDEA- language and IDE
2. Maven- dependency management
3. JDBC- java driver for DB
4. Spark- web framework, server
5. Velocity Engine- html template engine


**How to install and run server with web interface for db:**
>1.  You have to have all used technologies at your computer.
2.  Save all data from repository to your computer.
3.  Open this folder like a project in Intellij IDEA.
4.  Download all dependencies with Maven - go to 
le pom.xml and press "Import" if Intellij willsay that you need to import.
5.  Start PostgreSQL Server (with database from backup) from Intellij IDEA or pgAdmin.
6.  Run class Starter.java - it will run Spark server.
7.  Go tohttp://localhost:4567and test the site.